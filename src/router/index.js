import Vue from "vue";
import Router from "vue-router";
import User from "../views/UserPage.vue";
import ProductlistPage from "../views/ProductlistPage.vue";
import LoginPage from "../views/LoginPage.vue";
import UserlistPage from "../views/UserlistPage.vue"
import ShopPage from "../views/ShopPage.vue"

Vue.use(Router)

export default new Router({
        mode: "history",
        routes:[
            {
                path:"",
                component: LoginPage
            },
            {
                path: "/User",
                component: User,
                children:[
                    {
                        path:"/ProductlistPage",
                        component:ProductlistPage,
                    }, 
                    {
                        path:"/UserlistPage",
                        component:UserlistPage,
                    },
                    {
                        path:"/ShopPage",
                        component:ShopPage,
                    },
                ]
            },
        
        ]
    })




